﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace lab5 {
    public partial class Form1 : Form {
        interface IMyShape {
            string Text { get; set; }
            Font Font { get; set; }
            int Brush { get; set; }
            Color Color1 { get; set; }
            Color Color2 { get; set; }
            Color Color { get; set; }
            int Thickness { get; set; }
            void Reset(Point beg, Point end);
            void Draw(Graphics gr);
            bool Contains(Point pt);
        }

        class MyText : IMyShape {
            private Point _startPos;
            private SizeF _size;
            private Color _color1;
            private Color _color2;
            private Color _color;
            private int _brush;
            private int _thickness;
            private Font _font;
            private string _text;

            public string Text { get => _text; set => _text = value; }
            public SizeF Size1 { get => _size; set => _size = value; }
            public Point StartPos { get => _startPos; set => _startPos = value; }
            public Font Font { get => _font; set => _font = value; }
            public int Thickness { get => _thickness; set => _thickness = value; }
            public Color Color { get => _color; set => _color = value; }
            public Color Color1 { get => _color1; set => _color1 = value; }
            public Color Color2 { get => _color1; set => _color2 = value; }
            public int Brush { get => _brush; set => _brush = value; }

            public MyText(Point startPos, Color color, int brush, Font font, string text) {
                _startPos = startPos;
                _size = new SizeF(0, 0);
                _color = color;
                _brush = brush;
                _font = font;
                _text = text;
                _thickness = 30;
            }

            public void Reset(Point beg, Point end) {
                _startPos = beg;
                _size = new SizeF(end.X - beg.X, end.Y - beg.Y);
            }

            public bool Contains(Point pt) { return pt.X >= _startPos.X && pt.X <= _startPos.X + _size.Width && pt.Y >= _startPos.Y && pt.Y <= _startPos.Y + _size.Height; }

            public void Draw(Graphics gr) {
                Brush brush = new SolidBrush(_color);
                switch (_brush) {
                    case 2:
                        brush = new HatchBrush(HatchStyle.Cross, _color1);
                        break;
                    case 3: {
                        var path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent?.FullName + @"\Resources\texture.png";
                        brush = new TextureBrush(new Bitmap(path));
                        break;
                    }
                    case 4:
                        brush = new LinearGradientBrush(_startPos, new Point(_startPos.X + (int) _size.Width, _startPos.Y + (int) _size.Height), _color1, _color2);
                        break;
                }

                _font = new Font(_font.Name, _thickness);
                _size = gr.MeasureString(_text, _font);
                gr.DrawString(_text, _font, brush, _startPos.X, _startPos.Y);
            }
        }

        class MyEllipse : IMyShape {
            private Point _startPos;
            private int _width;
            private int _height;
            private Color _color;
            private int _thickness;
            private int _brush;
            private Color _color1;
            private Color _color2;
            public string Text { get; set; }
            public Font Font { get; set; }


            public Color Color1 { get => _color1; set => _color1 = value; }
            public Color Color2 { get => _color2; set => _color2 = value; }

            public int Brush { get => _brush; set => _brush = value; }

            public Color Color { get => _color; set => _color = value; }

            public int Thickness { get => _thickness; set => _thickness = value; }

            public MyEllipse(Point startPos, int width, int height, int thickness = 1, int brush = 0, Color color = default) {
                _startPos = startPos;
                _width = width;
                _height = height;
                _thickness = thickness;
                _brush = brush;
                _color = color;
            }

            public Point StartPos { get => _startPos; set => _startPos = value; }
            public int Width1 { get => _width; set => _width = value; }
            public int Height1 { get => _height; set => _height = value; }

            public void Reset(Point beg, Point end) {
                _startPos = beg;
                _width = end.X - beg.X;
                _height = end.Y - beg.Y;
            }

            public void Draw(Graphics gr) {
                if (_brush == 0) {
                    using (Pen pen = new Pen(_color, _thickness)) {
                        gr.DrawEllipse(pen, _startPos.X, _startPos.Y, _width, _height);
                    }
                } else {
                    switch (_brush) {
                        case 1: {
                            using (SolidBrush solidBrush = new SolidBrush(_color)) {
                                gr.FillEllipse(solidBrush, _startPos.X, _startPos.Y, _width, _height);
                            }

                            break;
                        }
                        case 2: {
                            using (HatchBrush hatchBrush = new HatchBrush(HatchStyle.Cross, _color1)) {
                                gr.FillEllipse(hatchBrush, _startPos.X, _startPos.Y, _width, _height);
                            }

                            break;
                        }
                        case 3: {
                            var path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent?.FullName + @"\Resources\texture.png";

                            using (TextureBrush textureBrush = new TextureBrush(new Bitmap(path))) {
                                gr.FillEllipse(textureBrush, _startPos.X, _startPos.Y, _width, _height);
                            }

                            break;
                        }
                        case 4: {
                            using (LinearGradientBrush linearGradientBrush = new LinearGradientBrush(_startPos, new Point(_startPos.X + _width, _startPos.Y + _height), _color1, _color2)) {
                                gr.FillEllipse(linearGradientBrush, _startPos.X, _startPos.Y, _width, _height);
                            }

                            break;
                        }
                    }
                }
            }

            public bool Contains(Point pt) {
                var minAxis = Math.Min(_height, _width) / 2;
                var maxAxis = Math.Max(_height, _width) / 2;
                var centerPoint = new Point((_startPos.X + _startPos.X + _width) / 2, (_startPos.Y + _startPos.Y + _height) / 2);
                var a = Math.Pow(pt.X - centerPoint.X, 2) / Math.Pow(maxAxis, 2);
                var b = Math.Pow(pt.Y - centerPoint.Y, 2) / Math.Pow(minAxis, 2);
                return a + b <= 1;
            }
        }

        /**
         * Class rectangle
         */
        class MyRectangle : IMyShape {
            private Point _pos;
            private int _width;
            private int _height;
            private Color _color;
            private int _thickness;
            private int _brush;
            private Color _color1;
            private Color _color2;
            public string Text { get; set; }
            public Font Font { get; set; }

            public Color Color1 { get => _color1; set => _color1 = value; }
            public Color Color2 { get => _color2; set => _color2 = value; }

            public int Brush { get => _brush; set => _brush = value; }

            public Color Color { get => _color; set => _color = value; }

            public int Thickness { get => _thickness; set => _thickness = value; }

            public Point Position { get => _pos; set => _pos = value; }
            public int WidthRect { get => _width; set => _width = value; }
            public int HeightRect { get => _height; set => _height = value; }

            public MyRectangle(Point pos, int width, int height, int thickness = 1, int brush = 0, Color color = default) {
                _pos = pos;
                _width = width;
                _height = height;
                _thickness = thickness;
                _brush = brush;
                _color = color;
                _color1 = _color;
                _color2 = _color;
            }

            public void Reset(Point beg, Point end) {
                _pos = beg;
                _width = end.X - beg.X;
                _height = end.Y - beg.Y;
            }

            public void Draw(Graphics gr) {
                if (_brush == 0) {
                    using (Pen pen = new Pen(_color, _thickness)) {
                        gr.DrawRectangle(pen, _pos.X, _pos.Y, _width, _height);
                    }
                } else {
                    switch (_brush) {
                        case 1: {
                            using (SolidBrush solidBrush = new SolidBrush(_color)) {
                                gr.FillRectangle(solidBrush, _pos.X, _pos.Y, _width, _height);
                            }

                            break;
                        }
                        case 2: {
                            using (HatchBrush hatchBrush = new HatchBrush(HatchStyle.Cross, _color1)) {
                                gr.FillRectangle(hatchBrush, _pos.X, _pos.Y, _width, _height);
                            }

                            break;
                        }
                        case 3: {
                            var path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent?.FullName + @"\Resources\texture.png";

                            using (TextureBrush textureBrush = new TextureBrush(new Bitmap(path))) {
                                gr.FillRectangle(textureBrush, _pos.X, _pos.Y, _width, _height);
                            }

                            break;
                        }
                        case 4: {
                            using (LinearGradientBrush linearGradientBrush = new LinearGradientBrush(_pos, new Point(_pos.X + _width, _pos.Y + _height), _color1, _color2)) {
                                gr.FillRectangle(linearGradientBrush, _pos.X, _pos.Y, _width, _height);
                            }

                            break;
                        }
                    }
                }
            }

            public bool Contains(Point pt) { return pt.X >= _pos.X && pt.X <= _pos.X + _width && pt.Y >= _pos.Y && pt.Y <= _pos.Y + _height; }
        }

        /**
         * Class line
         */
        class MyLine : IMyShape {
            private Point _startPoint;
            private Point _endPoint;
            private Color _color;
            private int _thickness;
            private int _brush;
            private Color _color1;
            private Color _color2;
            public string Text { get; set; }
            public Font Font { get; set; }

            public Color Color1 { get => _color1; set => _color1 = value; }
            public Color Color2 { get => _color2; set => _color2 = value; }

            public int Brush { get => _brush; set => _brush = value; }

            public Color Color { get => _color; set => _color = value; }

            public int Thickness { get => _thickness; set => _thickness = value; }

            public Point StartPoint { get => _startPoint; set => _startPoint = value; }
            public Point EndPoint { get => _endPoint; set => _endPoint = value; }

            public MyLine(Point startPoint, Point endPoint, int thickness = 1, Color color = default) {
                _startPoint = startPoint;
                _endPoint = endPoint;
                _thickness = thickness;
                _brush = 0;
                _color = color;
            }

            public void Reset(Point beg, Point end) {
                _startPoint = beg;
                _endPoint = end;
            }

            public void Draw(Graphics gr) {
                using (Pen pen = new Pen(_color, _thickness)) {
                    gr.DrawLine(pen, _startPoint, _endPoint);
                }
            }

            public bool Contains(Point pt) {
                var disPer = Math.Abs((_startPoint.Y - _endPoint.Y) * pt.X + (_endPoint.X - _startPoint.X) * pt.Y + _startPoint.X * _endPoint.Y - _endPoint.X * _startPoint.Y) / Math.Sqrt(Math.Pow(_startPoint.Y - _endPoint.Y, 2) + Math.Pow(_endPoint.X - _startPoint.X, 2));
                return disPer <= 2;
            }
        }

        private List<IMyShape> _data = new List<IMyShape>();
        private bool _isDrawing;
        private Point _curPoint;
        private Point _startPoint;
        private int _nSelected = -1;
        private bool _isLoad;
        private List<Color> _colors;
        private List<string> _fonts;

        enum EditMode { AddLine, AddRect, AddEllipse, AddSquare, AddCircle, AddText, Select }

        private EditMode _editMode;

        public Form1() {
            InitializeComponent();
            _colors = new List<Color>();
            _colors.Add(Color.Black);
            _colors.Add(Color.Red);
            _colors.Add(Color.Orange);
            _colors.Add(Color.Yellow);
            _colors.Add(Color.Blue);
            _colors.Add(Color.Green);
            _colors.Add(Color.Purple);

            _fonts = new List<string>();
            for (int i = 0; i < 11; i++) {
                _fonts.Add(FontFamily.Families[i].Name);
                cmbFont.Items.Add(FontFamily.Families[i].Name);
            }

            textIdSelected.Text = _nSelected.ToString();

            cmbAddEl.Items.Add("Ellipse");
            cmbAddEl.Items.Add("Square");
            cmbAddEl.Items.Add("Circle");
            cmbAddEl.Items.Add("Text");

            cmbAddEl.SelectedIndex = 0;
            cmbBrush.SelectedIndex = 0;
            cmbChangeColor.SelectedIndex = 0;
            editThickness.Text = 1.ToString();
            penBut.Checked = false;
            _editMode = EditMode.Select;
        }

        /**
         * Drawing Method
         */
        private void drawBox_Paint(object sender, PaintEventArgs e) {
            var i = 0;
            foreach (var el in _data) {
                if (i == _nSelected) {
                    if (_editMode == EditMode.Select && !_isLoad) {
                        el.Draw(e.Graphics);
                        textIdSelected.Text = _nSelected.ToString();
                    } else el.Draw(e.Graphics);
                } else el.Draw(e.Graphics);

                i++;
            }

            Int32.TryParse(editThickness.Text, out var thick);
            using (Pen pen = new Pen(_colors[cmbChangeColor.SelectedIndex], thick == 0 ? 1 : thick)) {
                if (_isDrawing) {
                    switch (_editMode) {
                        case EditMode.AddLine:
                            e.Graphics.DrawLine(pen, _startPoint, _curPoint);
                            break;
                        case EditMode.AddRect:
                            e.Graphics.DrawRectangle(pen, _startPoint.X, _startPoint.Y, _curPoint.X - _startPoint.X, _curPoint.Y - _startPoint.Y);
                            break;
                        case EditMode.AddEllipse:
                            e.Graphics.DrawRectangle(pen, _startPoint.X, _startPoint.Y, _curPoint.X - _startPoint.X, _curPoint.Y - _startPoint.Y);
                            break;
                        case EditMode.AddSquare:
                            e.Graphics.DrawRectangle(pen, _startPoint.X, _startPoint.Y, _curPoint.X - _startPoint.X, _curPoint.X - _startPoint.X);
                            break;
                        case EditMode.AddCircle:
                            e.Graphics.DrawRectangle(pen, _startPoint.X, _startPoint.Y, _curPoint.X - _startPoint.X, _curPoint.X - _startPoint.X);
                            break;
                    }
                }
            }
        }

        private void drawBox_MouseUp(object sender, MouseEventArgs e) {
            if (_isDrawing) {
                _curPoint = e.Location;
                Int32.TryParse(editThickness.Text, out var thick);
                switch (cmbAddEl.SelectedIndex) {
                    case 0:
                        _data.Add(new MyLine(_startPoint, _curPoint, thick == 0 ? 1 : thick, _colors[cmbChangeColor.SelectedIndex]));
                        break;
                    case 1:
                        _data.Add(new MyRectangle(_startPoint, _curPoint.X - _startPoint.X, _curPoint.Y - _startPoint.Y, thick == 0 ? 1 : thick, cmbBrush.SelectedIndex, _colors[cmbChangeColor.SelectedIndex]));
                        break;
                    case 2:
                        _data.Add(new MyEllipse(_startPoint, _curPoint.X - _startPoint.X, _curPoint.Y - _startPoint.Y, thick == 0 ? 1 : thick, cmbBrush.SelectedIndex, _colors[cmbChangeColor.SelectedIndex]));
                        break;
                    case 3:
                        _data.Add(new MyRectangle(_startPoint, _curPoint.X - _startPoint.X, _curPoint.X - _startPoint.X, thick == 0 ? 1 : thick, cmbBrush.SelectedIndex, _colors[cmbChangeColor.SelectedIndex]));
                        break;
                    case 4:
                        _data.Add(new MyEllipse(_startPoint, _curPoint.X - _startPoint.X, _curPoint.X - _startPoint.X, thick == 0 ? 1 : thick, cmbBrush.SelectedIndex, _colors[cmbChangeColor.SelectedIndex]));
                        break;
                    case 5: {
                        var font = new Font(cmbFont.Items[0].ToString(), 30);
                        _data.Add(new MyText(_startPoint, _colors[cmbChangeColor.SelectedIndex], cmbBrush.SelectedIndex, font, editText.Text));
                        break;
                    }
                }

                _nSelected = _data.Count - 1;
                textIdSelected.Text = _nSelected.ToString();
                trashBut.Enabled = true;
                _isDrawing = false;
                editThickness.Text = _data[_nSelected].Thickness.ToString();
                cmbChangeColor.SelectedIndex = _colors.IndexOf(_data[_nSelected].Color);
                drawBox.Refresh();
            }
        }

        private void drawBox_MouseDown(object sender, MouseEventArgs e) {
            _isDrawing = _editMode != EditMode.Select;
            _startPoint = e.Location;
        }

        private void drawBox_MouseMove(object sender, MouseEventArgs e) {
            if (_isDrawing && _curPoint != e.Location && _editMode != EditMode.AddText) {
                _curPoint = e.Location;
                drawBox.Refresh();
            }
        }

        private void cursorBut_Click(object sender, EventArgs e) {
            cursorBut.Checked = true;
            penBut.Checked = false;
            trashBut.Enabled = _editMode == EditMode.Select && _nSelected != -1;
            _editMode = EditMode.Select;
            textIdSelected.Text = _nSelected.ToString();
        }

        private void penBut_Click(object sender, EventArgs e) {
            cursorBut.Checked = false;
            penBut.Checked = true;
            trashBut.Enabled = false;

            switch (cmbAddEl.SelectedIndex) {
                case 0:
                    _editMode = EditMode.AddLine;
                    break;
                case 1:
                    _editMode = EditMode.AddRect;
                    break;
                case 2:
                    _editMode = EditMode.AddEllipse;
                    break;
            }

            _nSelected = -1;
            textIdSelected.Text = _nSelected.ToString();
            drawBox.Refresh();
        }


        private void drawBox_MouseClick(object sender, MouseEventArgs e) {
            if (_editMode == EditMode.Select && !_isLoad) {
                _nSelected = -1;

                var i = 0;
                foreach (var shape in _data) {
                    if (shape.Contains(e.Location)) {
                        _nSelected = i;
                        break;
                    }

                    i++;
                }

                if (_nSelected >= 0 && _nSelected < _data.Count) {
                    if (_data[_nSelected] is MyText) cmbAddEl.SelectedIndex = 5;
                    editThickness.Text = _data[_nSelected].Thickness.ToString();
                    cmbChangeColor.SelectedIndex = _colors.IndexOf(_data[_nSelected].Color);
                    trashBut.Enabled = true;
                    drawBox.Refresh();
                }
            }
        }

        /**
         * Delete selected figure
         */
        private void trashBut_Click(object sender, EventArgs e) {
            _data.RemoveAt(_nSelected);
            trashBut.Enabled = false;
            _nSelected = -1;
            textIdSelected.Text = _nSelected.ToString();
            drawBox.Refresh();
        }

        /**
         * Save Method
         */
        private void saveToolStripMenuItem_Click(object sender, EventArgs e) {
            if (saveDialog.ShowDialog() == DialogResult.OK) {
                var fileName = saveDialog.FileName.Split('.');

                // save lines
                var data = _data.Where(s => s is MyLine).ToList();
                var json = JsonConvert.SerializeObject(data, Formatting.Indented);

                using (FileStream fileStream = new FileStream(fileName[0] + "L.json", FileMode.Create)) {
                    var array = Encoding.Default.GetBytes(json);
                    fileStream.Write(array, 0, array.Length);
                }

                // save rectangles
                data = _data.Where(s => s is MyRectangle).ToList();
                json = JsonConvert.SerializeObject(data, Formatting.Indented);

                using (FileStream fileStream = new FileStream(fileName[0] + "R.json", FileMode.Create)) {
                    var array = Encoding.Default.GetBytes(json);
                    fileStream.Write(array, 0, array.Length);
                }

                // save ellipses
                data = _data.Where(s => s is MyEllipse).ToList();
                json = JsonConvert.SerializeObject(data, Formatting.Indented);

                using (FileStream fileStream = new FileStream(fileName[0] + "E.json", FileMode.Create)) {
                    var array = Encoding.Default.GetBytes(json);
                    fileStream.Write(array, 0, array.Length);
                }
                
                // save ellipses
                data = _data.Where(s => s is MyText).ToList();
                json = JsonConvert.SerializeObject(data, Formatting.Indented);

                using (FileStream fileStream = new FileStream(fileName[0] + "T.json", FileMode.Create)) {
                    var array = Encoding.Default.GetBytes(json);
                    fileStream.Write(array, 0, array.Length);
                }
                
                MessageBox.Show(@"Збережння успішнe!!!", @"Save", MessageBoxButtons.OK);
            }
        }

        /**
         * Load method
         */
        private void loadToolStripMenuItem_Click(object sender, EventArgs e) {
            if (openDialog.ShowDialog() == DialogResult.OK) {
                _isLoad = true;
                var fileName = openDialog.FileName.Split('.');
                var name = fileName[0].Remove(fileName[0].Length - 1);
                if (fileName[1].Equals("json")) {
                    _data.Clear();
                    // load lines
                    using (StreamReader streamReader = new StreamReader(name + "L." + fileName[1])) {
                        var json = streamReader.ReadToEnd();

                        var lines = JsonConvert.DeserializeObject<List<MyLine>>(json);
                        _data.AddRange(lines!);
                    }

                    // load rectangles
                    using (StreamReader streamReader = new StreamReader(name + "R." + fileName[1])) {
                        var json = streamReader.ReadToEnd();

                        var rectangles = JsonConvert.DeserializeObject<List<MyRectangle>>(json);

                        _data.AddRange(rectangles!);
                    }

                    // load rectangles
                    using (StreamReader streamReader = new StreamReader(name + "E." + fileName[1])) {
                        var json = streamReader.ReadToEnd();

                        var ellipses = JsonConvert.DeserializeObject<List<MyEllipse>>(json);

                        _data.AddRange(ellipses!);
                    }
                    
                    // load rectangles
                    using (StreamReader streamReader = new StreamReader(name + "T." + fileName[1])) {
                        var json = streamReader.ReadToEnd();

                        var ellipses = JsonConvert.DeserializeObject<List<MyText>>(json);

                        _data.AddRange(ellipses!);
                    }

                    drawBox.Refresh();
                    _isLoad = false;
                    MessageBox.Show(@"Загрузка успішна!!!", @"Load", MessageBoxButtons.OK);
                } else MessageBox.Show(@"Виберіть JSON файл", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /**
         * Change mode of drawing
         */
        private void cmbAddEl_SelectedIndexChanged(object sender, EventArgs e) {
            _editMode = (EditMode) cmbAddEl.SelectedIndex;
            _nSelected = -1;
            textIdSelected.Text = _nSelected.ToString();
            cmbFont.Visible = _editMode == EditMode.AddText;
            editText.Visible = _editMode == EditMode.AddText;
            penBut.Checked = true;
            cursorBut.Checked = false;
            trashBut.Enabled = false;
        }

        /**
         * Change thickness for selected figure
         */
        private void editThickness_TextChanged(object sender, EventArgs e) {
            if (_editMode != EditMode.Select && _nSelected == -1) {
                _editMode = (EditMode) cmbAddEl.SelectedIndex;
                _nSelected = -1;
                textIdSelected.Text = _nSelected.ToString();
                penBut.Checked = true;
                cursorBut.Checked = false;
            } else {
                if (_nSelected != -1) {
                    Int32.TryParse(editThickness.Text, out var thick);
                    _data[_nSelected].Thickness = thick == 0 ? 1 : thick;
                    drawBox.Refresh();
                }
            }

            trashBut.Enabled = false;
        }

        /**
         * Change color for selected figure
         */
        private void cmbChangeColor_SelectedIndexChanged(object sender, EventArgs e) {
            if (_editMode != EditMode.Select && _nSelected == -1) {
                _editMode = (EditMode) cmbAddEl.SelectedIndex;
                _nSelected = -1;
                textIdSelected.Text = _nSelected.ToString();
                penBut.Checked = true;
                cursorBut.Checked = false;
            } else {
                if (_nSelected != -1) {
                    _data[_nSelected].Color = _colors[cmbChangeColor.SelectedIndex];
                    drawBox.Refresh();
                }
            }

            trashBut.Enabled = false;
        }

        /**
         * Change brush for selected figure
         */
        private void cmbBrush_SelectedIndexChanged(object sender, EventArgs e) {
            if (_nSelected >= 0 && _nSelected < _data.Count) {
                _data[_nSelected].Brush = cmbBrush.SelectedIndex;
                drawBox.Refresh();
                cmbColor1.Visible = cmbBrush.SelectedIndex == 4 || cmbBrush.SelectedIndex == 2;
                cmbColor2.Visible = cmbBrush.SelectedIndex == 4;
            }
        }

        private void cmbColor1_SelectedIndexChanged(object sender, EventArgs e) {
            if (_nSelected >= 0 && _nSelected <= _data.Count) {
                _data[_nSelected].Color1 = cmbColor1.SelectedIndex == 0 ? _data[_nSelected].Color : _colors[cmbColor1.SelectedIndex];
                drawBox.Refresh();
            }
        }

        private void cmbColor2_SelectedIndexChanged(object sender, EventArgs e) {
            if (_nSelected >= 0 && _nSelected < _data.Count) {
                _data[_nSelected].Color2 = cmbColor2.SelectedIndex == 0 ? _data[_nSelected].Color : _colors[cmbColor2.SelectedIndex];
                drawBox.Refresh();
            }
        }

        private void cmbFont_SelectedIndexChanged(object sender, EventArgs e) {
            if (_nSelected >= 0 && _nSelected < _data.Count) {
                if (_data[_nSelected] is MyText) {
                    _data[_nSelected].Font = new Font(_fonts[cmbFont.SelectedIndex], _data[_nSelected].Thickness);
                    drawBox.Refresh();
                }
            }
        }

        private void editText_TextChanged(object sender, EventArgs e) {
            if (_nSelected >= 0 && _nSelected < _data.Count) {
                if (_data[_nSelected] is MyText) {
                    _data[_nSelected].Text = editText.Text;
                    drawBox.Refresh();
                }
            }
        }
    }
}
