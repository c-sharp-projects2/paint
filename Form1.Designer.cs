﻿namespace lab5 {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.drawBox = new System.Windows.Forms.Panel();
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.textIdSelected = new System.Windows.Forms.ToolStripLabel();
            this.cursorBut = new System.Windows.Forms.ToolStripButton();
            this.penBut = new System.Windows.Forms.ToolStripButton();
            this.trashBut = new System.Windows.Forms.ToolStripButton();
            this.cmbAddEl = new System.Windows.Forms.ToolStripComboBox();
            this.editThickness = new System.Windows.Forms.ToolStripTextBox();
            this.cmbChangeColor = new System.Windows.Forms.ToolStripComboBox();
            this.cmbBrush = new System.Windows.Forms.ToolStripComboBox();
            this.cmbColor1 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbColor2 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbFont = new System.Windows.Forms.ToolStripComboBox();
            this.editText = new System.Windows.Forms.ToolStripTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.imageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveDialog = new System.Windows.Forms.SaveFileDialog();
            this.openDialog = new System.Windows.Forms.OpenFileDialog();
            this.toolBar.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // drawBox
            // 
            this.drawBox.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.drawBox.AutoSize = true;
            this.drawBox.BackColor = System.Drawing.SystemColors.Control;
            this.drawBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.drawBox.Location = new System.Drawing.Point(12, 27);
            this.drawBox.Name = "drawBox";
            this.drawBox.Size = new System.Drawing.Size(841, 394);
            this.drawBox.TabIndex = 0;
            this.drawBox.Paint += new System.Windows.Forms.PaintEventHandler(this.drawBox_Paint);
            this.drawBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.drawBox_MouseClick);
            this.drawBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.drawBox_MouseDown);
            this.drawBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.drawBox_MouseMove);
            this.drawBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.drawBox_MouseUp);
            // 
            // toolBar
            // 
            this.toolBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { this.textIdSelected, this.cursorBut, this.penBut, this.trashBut, this.cmbAddEl, this.editThickness, this.cmbChangeColor, this.cmbBrush, this.cmbColor1, this.cmbColor2, this.cmbFont, this.editText });
            this.toolBar.Location = new System.Drawing.Point(0, 424);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(865, 26);
            this.toolBar.TabIndex = 1;
            this.toolBar.Text = "toolStrip1";
            // 
            // textIdSelected
            // 
            this.textIdSelected.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.textIdSelected.Name = "textIdSelected";
            this.textIdSelected.Size = new System.Drawing.Size(50, 23);
            this.textIdSelected.Text = "1000";
            // 
            // cursorBut
            // 
            this.cursorBut.Image = global::lab5.Properties.Resources.Cursor1;
            this.cursorBut.Name = "cursorBut";
            this.cursorBut.Size = new System.Drawing.Size(23, 23);
            this.cursorBut.Click += new System.EventHandler(this.cursorBut_Click);
            // 
            // penBut
            // 
            this.penBut.Image = global::lab5.Properties.Resources.Pen2;
            this.penBut.Name = "penBut";
            this.penBut.Size = new System.Drawing.Size(23, 23);
            this.penBut.Click += new System.EventHandler(this.penBut_Click);
            // 
            // trashBut
            // 
            this.trashBut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.trashBut.Enabled = false;
            this.trashBut.Image = global::lab5.Properties.Resources.trash1;
            this.trashBut.Name = "trashBut";
            this.trashBut.Size = new System.Drawing.Size(23, 23);
            this.trashBut.Text = "toolStripButton1";
            this.trashBut.Click += new System.EventHandler(this.trashBut_Click);
            // 
            // cmbAddEl
            // 
            this.cmbAddEl.Items.AddRange(new object[] { "Line", "Rectangle" });
            this.cmbAddEl.Name = "cmbAddEl";
            this.cmbAddEl.Size = new System.Drawing.Size(75, 26);
            this.cmbAddEl.SelectedIndexChanged += new System.EventHandler(this.cmbAddEl_SelectedIndexChanged);
            // 
            // editThickness
            // 
            this.editThickness.Name = "editThickness";
            this.editThickness.Size = new System.Drawing.Size(50, 26);
            this.editThickness.Text = "1";
            this.editThickness.TextChanged += new System.EventHandler(this.editThickness_TextChanged);
            // 
            // cmbChangeColor
            // 
            this.cmbChangeColor.Items.AddRange(new object[] { "Black", "Red", "Orange", "Yellow", "Blue", "Green", "Purple" });
            this.cmbChangeColor.Name = "cmbChangeColor";
            this.cmbChangeColor.Size = new System.Drawing.Size(75, 26);
            this.cmbChangeColor.SelectedIndexChanged += new System.EventHandler(this.cmbChangeColor_SelectedIndexChanged);
            // 
            // cmbBrush
            // 
            this.cmbBrush.Items.AddRange(new object[] { "none", "solid", "hatch", "image", "color" });
            this.cmbBrush.Name = "cmbBrush";
            this.cmbBrush.Size = new System.Drawing.Size(75, 26);
            this.cmbBrush.SelectedIndexChanged += new System.EventHandler(this.cmbBrush_SelectedIndexChanged);
            // 
            // cmbColor1
            // 
            this.cmbColor1.Items.AddRange(new object[] { "Black", "Red", "Orange", "Yellow", "Blue", "Green", "Purple" });
            this.cmbColor1.Name = "cmbColor1";
            this.cmbColor1.Size = new System.Drawing.Size(75, 26);
            this.cmbColor1.Text = "Color 1";
            this.cmbColor1.Visible = false;
            this.cmbColor1.SelectedIndexChanged += new System.EventHandler(this.cmbColor1_SelectedIndexChanged);
            // 
            // cmbColor2
            // 
            this.cmbColor2.Items.AddRange(new object[] { "Black", "Red", "Orange", "Yellow", "Blue", "Green", "Purple" });
            this.cmbColor2.Name = "cmbColor2";
            this.cmbColor2.Size = new System.Drawing.Size(75, 26);
            this.cmbColor2.Text = "Color 2";
            this.cmbColor2.Visible = false;
            this.cmbColor2.SelectedIndexChanged += new System.EventHandler(this.cmbColor2_SelectedIndexChanged);
            // 
            // cmbFont
            // 
            this.cmbFont.Name = "cmbFont";
            this.cmbFont.Size = new System.Drawing.Size(121, 26);
            this.cmbFont.Text = "Font";
            this.cmbFont.Visible = false;
            this.cmbFont.SelectedIndexChanged += new System.EventHandler(this.cmbFont_SelectedIndexChanged);
            // 
            // editText
            // 
            this.editText.Name = "editText";
            this.editText.Size = new System.Drawing.Size(100, 26);
            this.editText.Text = "Text";
            this.editText.Visible = false;
            this.editText.TextChanged += new System.EventHandler(this.editText_TextChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { this.imageToolStripMenuItem });
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(865, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // imageToolStripMenuItem
            // 
            this.imageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { this.loadToolStripMenuItem, this.saveToolStripMenuItem });
            this.imageToolStripMenuItem.Name = "imageToolStripMenuItem";
            this.imageToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.imageToolStripMenuItem.Text = "Image";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // openDialog
            // 
            this.openDialog.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 450);
            this.Controls.Add(this.toolBar);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.drawBox);
            this.Name = "Form1";
            this.Text = "Drawing";
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.ToolStripTextBox editText;

        private System.Windows.Forms.ToolStripComboBox cmbFont;

        private System.Windows.Forms.ToolStripComboBox cmbColor2;

        private System.Windows.Forms.ToolStripComboBox cmbColor1;

        private System.Windows.Forms.ToolStripComboBox cmbBrush;

        private System.Windows.Forms.ToolStripComboBox cmbChangeColor;

        private System.Windows.Forms.ToolStripTextBox editThickness;

        private System.Windows.Forms.ToolStripLabel textIdSelected;

        private System.Windows.Forms.ToolStripComboBox cmbAddEl;

        private System.Windows.Forms.OpenFileDialog openDialog;

        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.SaveFileDialog saveDialog;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;

        private System.Windows.Forms.ToolStripButton trashBut;

        private System.Windows.Forms.ToolStripButton penBut;

        private System.Windows.Forms.ToolStripButton cursorBut;

        private System.Windows.Forms.ToolStrip toolBar;

        private System.Windows.Forms.Panel drawBox;

        #endregion
    }
}
